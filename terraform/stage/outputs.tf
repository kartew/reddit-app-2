output "app_external_ip" {
  value = module.app.app_external_ip
}

output "db_external_ip" {
  value = module.db.db_external_ip
}

output "db_private_ip" {
  value = module.db.db_private_ip
}
