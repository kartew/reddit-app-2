provider "google" {
  project = var.project
  region = var.region
}

module "cloud-storage" {
  source = "terraform-google-modules/cloud-storage/google"
  version = "1.7.2"
  project_id = var.project
  names = [
    "test-bucket"]
  prefix = "kartew"

}

output "storage-bucket_url" {
  value = module.cloud-storage.url
}
